export const initTimer = () => {
  console.log('timer test');
  console.log($('body'));
  if (!$('body').hasClass('page-template-free-consultation')) {
    return;
  }
  console.log('timer init');
  const fullTime = 420; // 100%
  const initTime = 251; // in seconds

  let leftTime = initTime;
  let progress = (leftTime / fullTime) * 100; // %

  const $progressBar = $('.js-progress');
  const $timeDisplay = $('.js-time-display');

  const getTimeInMinutes = timeInSeconds => {
    const minutes = Math.floor(timeInSeconds / 60);
    let seconds = timeInSeconds - minutes * 60;

    if (seconds < 10) {
      seconds = '0' + seconds;
    }

    $timeDisplay.html(`${minutes}:${seconds}`);
  };

  const calculateAndSetProgress = () => {
    progress = (leftTime / fullTime) * 100;
    $progressBar.css('width', `${progress}%`);
  };

  const tick = () => {
    console.log('tick');
    leftTime -= 1;
    calculateAndSetProgress();
    getTimeInMinutes(leftTime);
  };

  calculateAndSetProgress();
  getTimeInMinutes(leftTime);
  setInterval(tick, 1000);
  
};
