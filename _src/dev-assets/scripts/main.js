import slick from 'slick-carousel';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { initTimer } from "./_timer";

initTimer();

console.log('main.js imported');

(function($) {
$('#slideshow10 .slick').slick({
  slidesToShow: 1,
  dots: false,
  arrows: false,
  autoplay: true,
  mobileFirst: true,
  centerMode: true,
  centerPadding: '30px',
  responsive: [
    {
      breakpoint: 650,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 1200,
      settings: 'unslick'
    }
  ]
});

$(window).on('load', function() {
  const $sliders = $('.js-init-slider');

  const sliderOptions = {
    logosOne: {
      slidesToShow: 2,
      variableWidth: true,
      dots: false,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 1000,
      mobileFirst: true,
      infinite: true,
      responsive: [
        {
          breakpoint: 650,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3
          }
        }
      ]
    }
  };

  $sliders.each(function() {
    const sliderName = $(this).attr('data-slider');
    $(this).slick(sliderOptions[sliderName]);
  });
});

})(jQuery);


