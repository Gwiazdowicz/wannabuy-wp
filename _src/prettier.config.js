const baseConfig = require('./prettier.baseconfig');

module.exports = {
    ...baseConfig
};
