<?php
/*
 * Template name: "Darmowa konsultacja"
 * */
get_header();

?>

<?php
$title          = get_field('subpage-title', 269);
$subtitleBold   = get_field('subpage-subtitle-bold', 269);
$subtitle       = get_field('subpage-subtitle', 269);
$progressTitle  = get_field('progress-title', 269);
$progressTitle2 = get_field('progress-title2', 269);

$number  = get_field('number', 11);
$btntop  = get_field('btntop', 11);
$heroImg = get_field('hero-img', 11);
$slider  = get_field('slider-one', 20);

$bg_img = "background-image:url(${heroImg['url']})";
?>
    
    
    <section class="hero hero__sub" style="<?= $bg_img ?>">
        <div class="hero__content">
            <div class="hero__border borders">
                <div class="hero__top-box hero__top-box-sub">
                    <div class="hero__contact-box">
                        <div class="hero__phone"></div>
                        <a href="tel:48224300330" class="hero__contact-tel"><?php echo esc_attr($number); ?></a>
                        <a href="!#" class="hero__contact-btn">
                            <p class="hero__contact-btn-text"><?php echo esc_attr($btntop); ?></p>
                        </a>
                    </div>
                    <a href="<?= SITE_URL ?>" class="hero__logo"></a>
                </div>
                <div class="hero__progress-box">
                    <p class="hero__progress-text">Pospiesz się!<span class="hero__progress-text--red"> Liczba konsultacji ograniczona</span></p>
                    <div class="hero__bar">
                        <div class="hero__progress-bar">
                            <div class="hero__progress-level js-progress"></div>
                        </div>
                        <div class="hero__progress-timer js-time-display">
                        </div>
                    </div>
                </div>
                <div class="hero__container container">
                    <div class="hero__bottom-box">
                        <div class="hero__text-box">
                            <h1 class="hero__title a-title"><?php echo esc_attr($title); ?></h1>
                            <div class="hero__lead-wrapper u-pr">
                                <div class="hero__arrow-left-sub"></div>
                                <p class="hero__article hero__article-sub a-subtitle">
                                    <b><?php echo esc_attr($subtitleBold); ?></b> <?php echo esc_attr($subtitle); ?></p>
                                <div class="hero__arrow-right-sub"></div>
                            </div>
                            <div class="hero__form-box-sub">
                                <?php echo do_shortcode('[ninja_form id=3]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php


if ($slider): ?>
    <div class="partners partners__sub l-borders">
        <div class="l-container u-pr">
            <h2 class="partners__title">Widziani w</h2>
            <div data-slider="logosOne" class="js-init-slider partners__slider">
                <?php foreach ($slider as $image): ?>
                    <div class="partners__slider-slide">
                        <img class="partners__slider-item" src="<?php echo $image['image']['url']; ?>" alt="">
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif;
get_footer();


