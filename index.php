<?php 
get_header();


require_once(__DIR__.'/parts/hero.php');
require_once(__DIR__.'/parts/partners.php');
require_once(__DIR__.'/parts/article.php'); 
require_once(__DIR__.'/parts/offer.php');
require_once(__DIR__.'/parts/partners-two.php');

require_once(__DIR__.'/parts/clients.php');
require_once(__DIR__.'/parts/about.php');
require_once(__DIR__.'/parts/trends.php');
require_once(__DIR__.'/parts/partners-three.php');

require_once(__DIR__.'/parts/problems.php'); 
require_once(__DIR__.'/parts/about-two.php');
require_once(__DIR__.'/parts/partners-four.php');

get_footer();

?>




