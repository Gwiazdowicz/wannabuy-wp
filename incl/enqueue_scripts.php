<?php
  declare(strict_types=1);
  
  // Include custom jQuery
  // IMPORTANT: The handle needs to stay named 'jquery' for plugins to work! 
  function t4d_include_custom_jquery() {
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), null, false);

    // New since 5.0.0
    // Removes Gutenberg Blocks related styles
    wp_dequeue_style('wp-block-library');
  }
  add_action('wp_enqueue_scripts', 't4d_include_custom_jquery');


  // Enqueue scripts
  function t4d_enqueue_scripts() {
    if (file_exists(TEMPLATE_DIR . 'assets/js/main.min.js') || file_exists(TEMPLATE_DIR . 'assets/js/main.js')) {
      $filename = file_exists(TEMPLATE_DIR . 'assets/js/main.min.js') ? 'main.min.js' : 'main.js';
      wp_enqueue_script('t4d_main_js', JS . $filename, array(), null, false);
    }

  }
  add_action('wp_enqueue_scripts', 't4d_enqueue_scripts');



  
 


  
