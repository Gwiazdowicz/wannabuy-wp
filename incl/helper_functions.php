<?php
  declare(strict_types=1);

  if (!function_exists('is_localhost')) {

    function is_localhost() {
      $whitelist = array('127.0.0.1', '::1', 'localhost');
      if (in_array($_SERVER['REMOTE_ADDR'], $whitelist))
        return true;
      return false;
    }

    ;

  }
