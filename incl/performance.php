<?php
  declare(strict_types=1);
  
  
  function t4d_defer_scripts($tag, $handle) {

    $handles_to_defer = array(
      'jquery',
      't4d_main_js',
      'underscore',
	  'nf-front-end-deps',
	  'nf-front-end',
	  'backbone',
	  'backbone-marionette',
    );

    if (!in_array($handle, $handles_to_defer))
      return $tag;

    return str_replace(' src', ' defer="defer" src', $tag);
  }
  if (!is_admin()) {
      add_filter('script_loader_tag', 't4d_defer_scripts', 10, 2);
  }
  


  function t4d_async_scripts($tag, $handle) {

    $handles_to_async = array(
      '',
    );

    if (!in_array($handle, $handles_to_async)) 
      return $tag;

    return str_replace(' src', ' async="async" src', $tag);
  }
  if (!is_admin()) {
      add_filter('script_loader_tag', 't4d_async_scripts', 10, 2);
  }
