<style>
<?php     
    if (is_home() || is_front_page()) {
        require_once TEMPLATE_DIR . 'incl/head/partials/home.php';
    } elseif (is_page_template('templates/free-consultation.php')) {
        require_once TEMPLATE_DIR . 'incl/head/partials/subpage.php';
    }
?>    
    .js-init-slider {opacity: 0;transition: opacity 0.3s ease-out;}
    .js-init-slider.slick-initialized {opacity: 1;}
    .nf-form-cont {position:relative;}
    .nf-loading-spinner {position: absolute;top:100px;bottom:0;left:0;right:0;margin:auto!important;}    
</style>

