<?php
  declare(strict_types=1);

  // Enqueue styles
  function t4d_enqueue_styles() {
    if (file_exists(TEMPLATE_DIR . 'assets/css/style.min.css') || file_exists(TEMPLATE_DIR . 'assets/css/style.css')) {
      $filename = file_exists(TEMPLATE_DIR . 'assets/css/style.min.css') ? 'style.min.css' : 'style.css';
      wp_enqueue_style('t4d_main_css', CSS . $filename, array(), null, false);
    }
  }
  add_action('get_footer', 't4d_enqueue_styles');
