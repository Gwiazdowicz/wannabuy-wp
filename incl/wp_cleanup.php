<?php
declare(strict_types=1);

//  Hide admin bar for logged in users
show_admin_bar(false);


// Credits to https://wpengineer.com/1438/wordpress-header/ 
function t4d_head_cleanup()
{
    // Remove wp-embed
    // wp-embed.min.js - is used for the embed posts widget functionality
    if ( ! is_admin()) {
        wp_deregister_script('wp-embed');
    }


    //  Disable XML-RPC RSD link from WordPress Header
    //  WordPress adds EditURI to your site header, which is required if you are publishing post by third party tool.
    //  <link rel="EditURI" type="application/rsd+xml" title="RSD" href="">
    remove_action('wp_head', 'rsd_link');


    //  Remove 'generator' declaration from head
    //  <meta name="generator" content="WordPress 4.9.2">
    remove_action('wp_head', 'wp_generator');

    //  Remove wlwmanifest - this is used by Windows Live Writer and other blog clients
    //  <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="">
    remove_action('wp_head', 'wlwmanifest_link');

    //  Remove rel="shortlink"
    //  <link rel="shortlink" href="">
    remove_action('wp_head', 'wp_shortlink_wp_head', 10);

    // Remove emojis and everything related
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    add_filter('emoji_svg_url', '__return_false');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');

    // Remove adjacent post links
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);


    remove_action('wp_head', 'feed_links_extra', 3);
    add_filter('use_default_gallery_style', '__return_false');
    add_filter('show_recent_comments_widget_style', '__return_false');

    // Remove oembeds
    remove_action('wp_head', 'rest_output_link_wp_head', 10);
    remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
    remove_action('rest_api_init', 'wp_oembed_register_route');
    add_filter('embed_oembed_discover', '__return_false');
    remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
    remove_action('wp_head', 'wp_oembed_add_discovery_links');
    remove_action('wp_head', 'wp_oembed_add_host_js');
    remove_action('template_redirect', 'rest_output_link_header', 11, 0);


    //https://www.wpbeginner.com/plugins/how-to-disable-xml-rpc-in-wordpress/
    add_filter('xmlrpc_enabled', '__return_false');

    // Remove 'type="text/javascript"' and 'type="text/css"'
    function t4d_remove_type_attr($tag)
    {
        return preg_replace("/type=['\"]text\/(javascript|css)['\"]/", '', $tag);
    }

    add_filter('style_loader_tag', 't4d_remove_type_attr', 10, 2);
    add_filter('script_loader_tag', 't4d_remove_type_attr', 10, 2);

    //  Remove ?ver=x.x.x from styles and scripts
    //  It splits the string on '?' char at returns everything on the left side 	
    function t4d_cleanup_query_string_from_assets($src)
    {        
        if ($src) {
            $parts = explode('?', $src);
            return $parts[0];    
        }        
    }

//    add_filter('script_loader_src', 't4d_cleanup_query_string_from_assets', 15, 1);
//    add_filter('style_loader_src', 't4d_cleanup_query_string_from_assets', 15, 1);
}

add_action('init', 't4d_head_cleanup');
