<?php
declare(strict_types=1);

$logo       = get_field('footer-logo', 239);
$infoTitle  = get_field('footer-title', 239);
$number     = get_field('footer-tel', 239);
$infoTitle2 = get_field('footer-title2', 239);
$email      = get_field('footer-email', 239);
$btn        = get_field('footer-btn', 239);
?>

<section class="footer">
    <div class="footer__border borders">
        <div class="footer__container container">
            <div class="footer__top-block">
                <div class="footer__box">
                    <div class="footer__image"></div>
                </div>
                <div class="footer__box">
                    <p class="footer__title"><?php echo esc_attr($infoTitle); ?></p>
                    <a href="tel:48224300330" class="footer__subtitle footer__link"><?php echo esc_attr($number); ?></a>
                </div>
                <div class="footer__box">
                    <p class="footer__title"><?php echo esc_attr($infoTitle2); ?></p>
                    <a href="mailto:kontakt@wannaubuy.pl" class="footer__subtitle footer__link"><?php echo esc_attr($email); ?></a>
                </div>
                <div class="footer__box">
                    <div class="footer__logo-box">
                        <a href="#!" class="footer__logo footer__logo--tweet"></a>
                        <a href="#!" class="footer__logo footer__logo--fb"></a>
                    </div>
                    <div class="footer__copy">2020 © Made with passion by Time4</div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom-block">
        <div class="footer__num"><?php echo esc_attr($number); ?></div>
        <a href="<?= DARMOWA_KONSULTACJA ?>" class="footer__btn btn-ga-advice-alone">
            <p><?php echo esc_attr($btn); ?></p>
        </a>
    </div>
</section>
<?php
    wp_footer();
?>
</div>
</body>
</html>
