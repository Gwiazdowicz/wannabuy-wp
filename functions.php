<?php

declare(strict_types=1);
  
define('TEMPLATE_DIR', get_template_directory() . '/');
define('DARMOWA_KONSULTACJA', site_url(). '/darmowa-konsultacja/');

require_once(TEMPLATE_DIR . 'incl/helper_functions.php');
require_once(TEMPLATE_DIR . 'incl/constants.php');
require_once(TEMPLATE_DIR . 'incl/site_setup.php');
require_once(TEMPLATE_DIR . 'incl/enqueue_scripts.php');
require_once(TEMPLATE_DIR . 'incl/enqueue_styles.php');
require_once(TEMPLATE_DIR . 'incl/wp_cleanup.php');
require_once(TEMPLATE_DIR . 'incl/performance.php');

function zwp_add_menu() {
  register_nav_menu( 'MainMenu', 'Menu główne' );
}
add_action('init','zwp_add_menu');

