<?php
$title = get_field('about2-title',116 );
$titlesmall = get_field('about2-title-small', 116);
$titletop = get_field('about2-title-top', 116);
$aboutext = get_field('about2-text', 116);
$aboubtn = get_field('about2-btn',116 );
$aboutBold = get_field('about2-bold',116 );

$aboutText = get_field('about2-text1', 116);
$aboutText2 = get_field('about2-text2', 116);
$aboutText3 = get_field('about2-text3', 116);
$aboutText4 = get_field('about2-text4', 116);
?>

<section class="about">
    <div class="about__border borders">
        <div class="about__container container">
            <div class="about__box --observe appear-up">
                <div class="about__content">
                    <h2 class="about__title-top-two a-title-two a-title-two--red"><?php echo esc_attr( $titletop ); ?></h2>
                    <h2 class="about__title a-title-two a-title-two--red"><?php echo esc_attr( $titlesmall ); ?></h2>
                </div>
                <div class="about__content-big">
                <h2 class="about__title a-title-two a-title-two--red"><?php echo esc_attr( $title ); ?></h2>
                </div>
                <div class="about__content-bottom">
                     <div class="about__box-one">
                        <p class="a-article with-arrows arrow-left"><?php echo esc_attr( $aboutText ); ?></p>
                        <p class="a-article"><b><?php echo esc_attr( $aboutBold  ); ?></b><?php echo esc_attr( $aboutText2 ); ?></p>
                        <p class="a-article"><?php echo esc_attr( $aboutText3 ); ?></p>
                        <p class="a-article with-arrows arrow-right"><?php echo esc_attr( $aboutText4  ); ?></p>
                    </div>     
                </div>
            </div>
            <div class="about__btn">
                <a href="<?= DARMOWA_KONSULTACJA; ?>" class="about__btn-text a-btn-two btn-ga-advice"><?php echo esc_attr( $aboubtn ); ?></a>
            </div>
        </div>    
    </div>    
</section>
