<?php
$title = get_field('about-title', 83);
$boldText = get_field('about-bold', 83);
$boldText2 = get_field('about-bold2', 83);
$titlesmall = get_field('about-title-small', 83);
$titletop = get_field('about-title-top', 83);
$aboutext = get_field('about-text', 83);
$aboutexttwo = get_field('about-text-2', 83);
$aboutextthree = get_field('about-text-3', 83);
$aboutextfour = get_field('about-text-4', 83);
$aboutextfive = get_field('about-text-5', 83);
$aboubtn = get_field('about-btn', 83);
?>


<section class="about">
    <div class="about__border borders">
        <div class="about__container container">
            <div class="about__box --observe appear-up">
                <div class="about__content">
                    <h2 class="about__title-top a-title-two a-title-two--red"><?php echo esc_attr( $titletop ); ?></h2>
                    <h2 class="about__title a-title-two a-title-two--red"><?php echo esc_attr( $titlesmall ); ?></h2>
                </div>
                <div class="about__content-big">
                    <h2 class="about__title a-title-two a-title-two--red"><?php echo esc_attr($title);?></h2>
                </div>
                <div class="about__content-bottom">
                    <div class="about__box-one">
                        <p class="with-arrows arrow-left a-article"><b><?php echo esc_attr( $boldText ); ?></b><?php echo esc_attr( $aboutexttwo ); ?> </p>
                        <p class="a-article"><?php echo esc_attr( $aboutextfive); ?></p>
                        <p class="a-article"><?php echo esc_attr( $aboutextthree ); ?></p>
                        <p class="with-arrows arrow-right a-article"><?php echo esc_attr( $aboutextfour ); ?><b><?php echo esc_attr( $boldText2 ); ?></b></p>
                    </div>   
                </div>
            </div>
            <div class="about__btn">
                <a href="<?= DARMOWA_KONSULTACJA; ?>" class="about__btn-text a-btn-two btn-ga-advice"><?php echo esc_attr( $aboubtn );?></a>
            </div>
        </div>    
    </div>    
</section>

