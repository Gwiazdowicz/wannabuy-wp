<?php
$image = get_field('img-trends',97);
$title = get_field('title-trends', 97);
$subtitle = get_field('subtitle-trends',97);
$text = get_field('text-trends',97);
$btn= get_field('btn-trends',97);
?>

<section class="trends">
    <div class="trends__borders">
        <div class="trends__container">
            <div class="trends__content">
                <div class="trends__col-one u-pr">
                    
                <div class="trends__book-wrapper u-pr">
                    <div class="trends__book-main">                          
                        </div>
                        <img class="trends__arrow-1" src="<?= IMAGES . 'book-arrow-1.png' ?>" alt="">
                        <img class="trends__arrow-2" src="<?= IMAGES . 'book-arrow-2.png' ?>" alt="">
                        <img class="trends__arrow-3" src="<?= IMAGES . 'book-arrow-3.png' ?>" alt="">
                    </div>
                </div>
                <div class="trends__col-two">
                    <div class="trends__text-box">
                        <h2 class="trends__title --observe appear-up"><?php echo esc_attr( $title);?></h2>
                        <p class="trends__subtitle --observe appear-up"><?php echo esc_attr( $subtitle);?></p>
                        <p class="trends__article a-article --observe appear-up"><?php echo esc_attr( $text);?></p>
                    </div>

                    <div class="trends__form-box --observe appear-up">
                         <label for="male">Wypełnij formularz</label>
                         <div class="trends__form-title"><?php echo do_shortcode('[ninja_form id=2]');?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
