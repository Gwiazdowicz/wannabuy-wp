<?php
$slider = get_field('slider-two', 124);

if ($slider): ?>
    
    <div class="partners l-borders">
        <div class="l-container u-pr">
            <h2 class="partners__title">Zaufali nam</h2>
            <div data-slider="logosOne" class="js-init-slider partners__slider">
                <?php foreach ($slider as $image): ?>
                    <img class="partners__slider-item" src="<?php echo $image['image']['url']; ?>" alt="">
                <?php endforeach; ?>
            </div>
        </div>
    </div>

<?php endif; 

