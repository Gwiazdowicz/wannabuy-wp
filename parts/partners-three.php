<?php
$slider = get_field('slider-three', 131);
if ($slider): ?>
    
    <div class="partners l-borders partners--three">
        <div class="l-container">
            <h2 class="partners__title">Dostępne na Wannabuy</h2>
            <div data-slider="logosOne" class="js-init-slider partners__slider">
                <?php foreach ($slider as $image): ?>
                    <img class="partners__slider-item" src="<?php echo $image['image']['url']; ?>" alt="">
                <?php endforeach; ?>
            </div>
        </div>
    </div>

<?php endif; 
