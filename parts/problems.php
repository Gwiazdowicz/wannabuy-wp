<?php
$title= get_field( 'title-problems', 106);
$subtitle= get_field('subtitle-problems', 106);
$text= get_field('text-problems',106);
$background = get_field('background-problems', 106);
?>

<section class="problems">
    <img class="problems__background" src="<?php echo $background['url']; ?>" alt="<?php echo $image['alt']; ?>">
        <div class="problems__border borders">
            <div class="problems__container container">
                <h2 class="problems__title a-title-two a-title--white --observe appear-up"><?php echo esc_attr( $title);?></h2>
                <p class="problems__subtitle --observe appear-up"><?php echo esc_attr( $subtitle);?></p>
                <p class="problems__box-one a-article a-article--white --observe appear-up"> <?php echo esc_attr( $text);?></p>
            </div>
        </div>    
</section>
