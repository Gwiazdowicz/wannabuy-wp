<?php
$title    = get_field('title', 11);
$subtitle = get_field('subtitle', 11);
$number   = get_field('number', 11);
$btntop   = get_field('btntop', 11);
$btnform  = get_field('btnform', 11);
$heroImg  = get_field('hero-img', 11);
if ($title): ?>
    
    <section class="hero --observe">
        <img class="hero__background" src="<?php echo $heroImg['url']; ?>" alt="<?php echo $image['alt']; ?>">
        <div class="hero__content">
            <div class="hero__border borders">
                <div class="hero__top-box">
                    <div class="hero__contact-box">
                        <div class="hero__phone"></div>
                        <a href="tel:48224300330" class="hero__contact-tel"><?php echo esc_attr($number); ?></a>
                        <a href="<?= DARMOWA_KONSULTACJA; ?>" class="hero__contact-btn btn-ga-advice-alone">
                            <p class="hero__contact-btn-text"><?php echo esc_attr($btntop); ?></p>
                        </a>
                    </div>
                    <a href="<?= SITE_URL ?>" class="hero__logo"></a>
                </div>
                <div class="hero__container container">
                    <div class="hero__bottom-box">
                        <div class="hero__text-box">
                            <h1 class="hero__title a-title"> <?php echo esc_attr($title); ?></h1>
                            <div class="hero__lead-wrapper u-pr">
                                <div class="hero__arrow-left"></div>
                                <p class="hero__article a-subtitle"><?php echo esc_attr($subtitle); ?></p>
                                <div class="hero__arrow-right"></div>
                            </div>
                            <div class="hero__form-box">
                                <?php echo do_shortcode('[ninja_form id=1]'); ?></div>
                        </div>
                        <div class="hero__mouse"></div>
                    </div>
                </div>
            </div>
        </div>    
    </section>
<?php endif;
?>
