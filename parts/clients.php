<?php
$title = get_field('titleclients', 64);
$imageSlider = get_field('sliderclients', 64);
$background = get_field('backgroundclients', 64);
?>


<?php 
if( $imageSlider ): ?>
<section class="clients">
    <img class="clients__background" src="<?php echo $background['url']; ?>" alt="<?php echo $image['alt']; ?>">
        <div class="clients__border borders">
            <div class="clients__container container">
                <div class="clients__title-box">
                    <h2 class="clients__title a-title-two a-title-two--white"><?php echo esc_attr( $title ); ?></h2>
                </div>
                <div id="slideshow10">
                    <div class="slick slider-three">
                    <?php while( have_rows('sliderclients',64)): the_row(); 
                                $image= get_sub_field('clients-img');
                                $comment = get_sub_field('clients-coment');
                                $name = get_sub_field('clients-name');
                                $job = get_sub_field('clients-job');
                                ?>


                            <div class="clients__tile">
                                <div class="clients__tile-block">
                                    <div class="clients__symbol"></div>
                                    <p class="clients__text"><?php echo $comment;?></p>
                                    <p class="clients__name"><b><?php echo $name;?></b><?php echo $job;?></p>
                                    <div class="clients__logo"><img class="clients__logo-img"  src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
                                </div> 
                            </div>
                        <?php endwhile; ?> 
                    </div>
                </div>
            </div>
        </div>    
</section>

<?php endif; ?>

