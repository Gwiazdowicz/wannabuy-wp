<?php
$title = get_field('title-offer', 48);
$boxOne = get_field('box-one', 48);
$boxTwo = get_field('box-two', 48);
$boxThree = get_field('box-three', 48);
$boxFour = get_field('box-four', 48);
$iconOne = get_field('icon-one', 48);
$iconTwo = get_field('icon-two', 48);
$iconThree = get_field('icon-three', 48);
$iconFour = get_field('icon-four', 48);
if( $title ): ?>


<section class="offer">
    <div class="offer__border borders">
        <div class="offer__container container">
            <div class="offer__block">
                <div class="offer__text-box">
                    <h2 class="offer__title a-title-two --observe appear-up"><?php echo esc_attr( $title ); ?></h2>
                </div>
                <div class="offer__tiles-box">
                    <div class="offer__tile --observe appear-up">
                        <div class="offer__tile-icon-box"><img class="offer__tile-icon" src="<?php echo $iconOne['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
                        <p class="offer__tile-text"><?php echo esc_attr( $boxOne ); ?></p>
                    </div>
                    <div class="offer__tile --observe appear-up">
                        <div class="offer__tile-icon-box"><img class="offer__tile-icon" src="<?php echo $iconTwo['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
                        <p class="offer__tile-text"><?php echo esc_attr( $boxTwo ); ?></p>
                    </div>
                    <div class="offer__tile --observe appear-up">
                        <div class="offer__tile-icon-box"><img class="offer__tile-icon" src="<?php echo $iconThree['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
                        <p class="offer__tile-text"><?php echo esc_attr( $boxThree ); ?></p>
                    </div>
                    <div class="offer__tile --observe appear-up">
                        <div class="offer__tile-icon-box"><img class="offer__tile-icon" src="<?php echo $iconFour['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
                        <p class="offer__tile-text"><?php echo esc_attr( $boxFour ); ?></p>
                    </div>
                </div>
            </div>
        </div>
   </div>
</section>

<?php endif; 
?>