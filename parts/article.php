<?php
$title = get_field('title-article', 38);
$text = get_field('text-article', 38);
$text2 = get_field('text-article2', 38);
$text3 = get_field('text-article3', 38);
$text4 = get_field('text-article4', 38);
$text5 = get_field('text-article5', 38);
$btn = get_field('btn-article', 38);


if( $title ): ?>
<section class="article">
        <div class="article__border borders">
            <div class="article__container container">
                <h2 class="article__title a-title-two --observe appear-up">Drogi pracodawco,</h2>
                <div class="article__box-one">
                    <p class="a-article --observe appear-up">Wyobraź sobie, że możesz zatrzymać swoich pracowników na dłużej, zmniejszyć wydatki poświęcane na nieustanną rekrutację i&nbsp;po prostu skupić się swoim biznesie - brzmi jak piękny sen prawda? Nie do końca, jesteśmy w&nbsp;stanie zmienić wizję w&nbsp;rzeczywistość.</p>
                    <p class="a-article --observe appear-up">Zamiast ciągle rekrutować, onboardować nowych ludzi oraz ciągle szukać sposobów na zaopiekowanie się istniejącą kadrą mógłbyś wreszcie położyć pełny nacisk na to czym Twoja firma rzeczywiście się zajmuje - w&nbsp;towarzystwie swojego zaufanego i&nbsp;doświadczonego zespołu.</p>
                    <p class="a-article --observe appear-up">Nasze strategie, benefity oraz system WannaBuy codziennie wspierają firmy takie jak Coca-Cola, Shell, Danone czy Mokate odciążając ich od myślenia o&nbsp;problemach związanych z&nbsp;migracją pracowników.</p>
                    <p class="a-article --observe appear-up">Nie skupiamy się na jednej wybranej niszy, wspieramy różne branże, a&nbsp;nasze doświadczenie pozwala dopasować rozwiązania tam gdzie Twój biznes tego potrzebuje.</p>
                    <p class="a-article --observe appear-up">Trudno w&nbsp;to uwierzyć, zdajemy sobie sprawę - nie musisz wierzyć nam na słowo, sprawdź niżej, co mówią o&nbsp;nas nasi klienci lub po prostu skontaktuj się z&nbsp;nami na bezpłatną konsultację, gdzie zdiagnozujemy Twoje problemy kadrowe i&nbsp;zaproponujemy rozwiązanie - Ty sam zdecydujesz, czy jesteś gotowy zacząć.</p>
                </div>
                
                <a href="<?= DARMOWA_KONSULTACJA; ?>" class="article__btn a-btn-two --observe appear-up btn-ga-advice">
                    <div  class="article__btn-text ">Odbierz 30 minutową darmową konsultację HR </div>
                </a>
            </div>
        </div>    
</section>
<?php endif; 
?>




 

