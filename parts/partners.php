<!-- ACF fields stop working due to js errors, change get_field and foreach to simple for -->
<?php
$slider = get_field('slider-one', 20);

//if ($slider): ?>
    <div class="partners l-borders">
        <div class="l-container u-pr">
            <h2 class="partners__title">Widziani w</h2>
            <div data-slider="logosOne" class="js-init-slider partners__slider">
                <?php for ($i=1; $i < 8 ; $i++) { ?>
                    <img class="partners__slider-item" src="<?= TEMPLATE_DIR; ?>assets/img/seen-<?= $i; ?>.png" alt="">
                <?php } ?>

                <!-- <?php foreach ($slider as $image): ?>
                    <img class="partners__slider-item" src="<?php echo $image['image']['url']; ?>" alt="">
                <?php endforeach; ?> -->
            </div>
        </div>
    </div>
<?php //endif; 
